package com.example.kalkulatorusm.rumus;

import android.content.Context;

public class Temperaturespanjang {
    private Context context;

    public Temperaturespanjang(Context context) {
        this.context = context;
    }

    /**
     * Kilometer
     **/
    // formula conversion kilometer ke mili
    public String KilometerToMilimeter(double kilometer) {
        // R = Kilomeetr *1000000
        double R = kilometer * 1000000;
        return check_after_decimal_point(R);
    }

    // formula conversion kilometer to Meter
    public String KilometerToMeter(double kilometer) {
        // F = kilometer * 1000
        double F = kilometer * 1000;
        return check_after_decimal_point(F);
    }

    // formula conversion Kilometer to Cm
    public String KilometerToCentimeter(double kilometer) {
        double K = kilometer * 100000;
        return check_after_decimal_point(K);
    }

    /**
     * Milimeter
     **/
    // Mm to Km
    public String MilimeterToKilometer(double mili) {
        double C = mili / 1000000;
        return check_after_decimal_point(C);
    }

    // Mm to M
    public String MiliToMeter(double mili) {
        double F = mili / 1000;
        return check_after_decimal_point(F);
    }

    // Mm to Cm
    public String MiliToCentimeter(double mili) {
        double K = mili / 10;
        return check_after_decimal_point(K);
    }

    /**
     * Meter
     **/
    // M to Km
    public String MeterToKilometer(double meter) {
        double C = meter / 1000;
        return check_after_decimal_point(C);
    }

    // M to Mm
    public String MeterToMili(double meter) {
        double R = meter * 1000;
        return check_after_decimal_point(R);
    }

    // M to Cm
    public String MeterToCentimeter(double meter) {
        double K = meter * 100;
        return check_after_decimal_point(K);
    }

    /**
     * Centimeter
     **/
    // Cm to Km
    public String CentimeterTokilometer(double cm) {
        double K = cm / 100000;
        return check_after_decimal_point(K);
    }

    // Cm to Mm
    public String CentimeterTomili(double cm) {
        double K = cm * 10;
        return check_after_decimal_point(K);
    }

    // Cm to M
    public String CentimeterToMeter(double cm) {
        double K = cm / 100;
        return check_after_decimal_point(K);
    }

    //check after decimal point
    public String check_after_decimal_point(double decimal) {
        String result = null;
        String[] after_point = String.valueOf(decimal).split("[:.]");
        if (after_point[after_point.length - 1].equals("0")) {
            result = after_point[0];
        } else {
            result = String.valueOf(decimal).replace(".", ",");
        }
        return result;
    }
}