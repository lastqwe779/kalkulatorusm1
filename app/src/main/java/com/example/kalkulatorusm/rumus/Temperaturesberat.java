package com.example.kalkulatorusm.rumus;

import android.content.Context;

public class Temperaturesberat {
    private Context context;

    public Temperaturesberat(Context context) {
        this.context = context;
    }

    /**
     * Kilogram
     **/
    // formula conversion kilogram ke gram
    public String KilogramToGram(double kilogram) {
        // R = 1000 * kilogram
        double R = kilogram * 1000;
        return check_after_decimal_point(R);
    }

    // formula conversion kilogram to q
    public String KilogramToKwintal(double kilogram) {
        // F = 100 / kilogram
        double F = kilogram / 100;
        return check_after_decimal_point(F);
    }

    // formula conversion Kilogram to Ton
    public String KilogramToTon(double kilogram) {
		//Riski Apriyanto G.111.20.0051
        double K = kilogram / 1000;
        return check_after_decimal_point(K);
    }

    /**
     * Kwintal
     **/
    // q to Kg
    public String KwintalToKilogram(double kwintal) {
        // rumus : C = q * 100
        double C = kwintal * 100;
        return check_after_decimal_point(C);
    }

    // q to g
    public String KwintalToGram(double kwintal) {
        double F = kwintal * 100000;
        return check_after_decimal_point(F);
    }

    // q to T
    public String KwintalToTon(double kwintal) {
        double K = kwintal / 10;
        return check_after_decimal_point(K);
    }

    /**
     * Ton
     **/
    // T to Kg
    public String TonToKilogram(double ton) {
        double C = ton * 1000;
        return check_after_decimal_point(C);
    }

    // T to G
    public String TonToGram(double ton) {
        double R = ton * 1000000;
        return check_after_decimal_point(R);
    }

    // t to q
    public String TonTokwintal(double ton) {
        double K = ton * 10;
        return check_after_decimal_point(K);
    }

    /**
     * Gram
     **/
    // G to Kg
    public String GramTokilogram(double gram) {
        double K = gram / 1000;
        return check_after_decimal_point(K);
    }

    // G to q
    public String GramTokwintal(double gram) {
        double K = gram / 100000;
        return check_after_decimal_point(K);
    }

    // G to T
    public String GramToTon(double gram) {
        double K = gram / 1000000;
        return check_after_decimal_point(K);
    }

    //check after decimal point
    public String check_after_decimal_point(double decimal) {
        String result = null;
        String[] after_point = String.valueOf(decimal).split("[:.]");
        if (after_point[after_point.length - 1].equals("0")) {
            result = after_point[0];
        } else {
            result = String.valueOf(decimal).replace(".", ",");
        }
        return result;
    }
}
