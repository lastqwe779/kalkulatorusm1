package com.example.kalkulatorusm;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kalkulatorusm.R;
import com.example.kalkulatorusm.rumus.SharedPrefsTemphpanjang;
import com.example.kalkulatorusm.rumus.Temperaturespanjang;

import java.util.Objects;

public class KonversiPanjang extends AppCompatActivity {

    ActionBar back3;

    private AppCompatEditText edit_temppanjang1;
    private AppCompatEditText edit_temppanjang2;
    private Temperaturespanjang temperaturespanjang;
    private LinearLayout layout_formulapanjang;
    private TextView text_formulapanjang;
    private Animation rotate_zoom_out;
    private String[] temperatures = new String[]{
            "\u00B0Km",
            "\u00B0Mm",
            "\u00B0M",
            "\u00B0Cm",
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konversi_panjang);


        temperaturespanjang = new Temperaturespanjang(this);

        layout_formulapanjang = findViewById(R.id.layout_formulapanjang);
        text_formulapanjang = findViewById(R.id.text_formulapanjang);

        rotate_zoom_out = AnimationUtils.loadAnimation(KonversiPanjang.this, R.anim.rotate_zoom_out);

        //edit text temp to conversion 1
        edit_temppanjang1 = findViewById(R.id.edit_1_temperature_to_conversionpanjang);
        edit_temppanjang1.setHint(SharedPrefsTemphpanjang.getTempSymbol1(KonversiPanjang.this));

        //edit text temp to conversion 2
        edit_temppanjang2 = findViewById(R.id.edit_2_temperature_to_conversionpanjang);
        edit_temppanjang2.setHint(SharedPrefsTemphpanjang.getTempSymbol2(KonversiPanjang.this));
        edit_temppanjang2.setKeyListener(null);
        edit_temppanjang2.setClickable(false);

        //Spinner & Adapter 1
        ArrayAdapter<String> arrayAdapter1 =
                new ArrayAdapter(KonversiPanjang.this, android.R.layout.simple_spinner_item, temperatures);
        arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AppCompatSpinner appCompatSpinner1 = findViewById(R.id.spinner_1_temperature_to_conversionpanjang);
        appCompatSpinner1.setAdapter(arrayAdapter1);

        //set selection
        appCompatSpinner1.setSelection(SharedPrefsTemphpanjang.getTempIndex1(KonversiPanjang.this));
        appCompatSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String temp_symbol1 = temperatures[position];
                SharedPrefsTemphpanjang.setTemperature1(KonversiPanjang.this, temp_symbol1, position);
                edit_temppanjang1.setHint(temperatures[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        back3 = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //spinner & adapter 2
        ArrayAdapter<String> arrayAdapter2 =
                new ArrayAdapter(KonversiPanjang.this, android.R.layout.simple_spinner_item, temperatures);
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AppCompatSpinner appCompatSpinner2 = findViewById(R.id.spinner_2_temperature_to_conversionpanjang);
        appCompatSpinner2.setAdapter(arrayAdapter2);
        appCompatSpinner2.setSelection(SharedPrefsTemphpanjang.getTempIndex2(KonversiPanjang.this));
        appCompatSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String temp_symbol2 = temperatures[position];
                SharedPrefsTemphpanjang.setTemperature2(KonversiPanjang.this, temp_symbol2, position);
                edit_temppanjang2.setHint(temperatures[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Button Count
        AppCompatButton btn_countpanjang = findViewById(R.id.countpanjang);
        btn_countpanjang.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (Objects.requireNonNull(edit_temppanjang1.getText()).toString().isEmpty()) {

                    Toast.makeText(KonversiPanjang.this,
                            "Masukan nilai suhu yang ingin di konversi", Toast.LENGTH_SHORT).show();

                } else {
                    layout_formulapanjang.startAnimation(rotate_zoom_out);
                    if (layout_formulapanjang.getVisibility() == View.GONE) {
                        layout_formulapanjang.setVisibility(View.VISIBLE);
                    }
                    String symbol_temp1 = SharedPrefsTemphpanjang.getTempSymbol1(KonversiPanjang.this);
                    String symbol_temp2 = SharedPrefsTemphpanjang.getTempSymbol2(KonversiPanjang.this);
                    double value_to_conversion = Double.parseDouble(edit_temppanjang1.getText().toString());

                    // Km to Mm
                    if (symbol_temp1.equals("\u00B0Km") && symbol_temp2.equals("\u00B0Mm")) {
                        edit_temppanjang2.setText(temperaturespanjang.KilometerToMilimeter(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.KilometerToMilimeter(value_to_conversion));
                    }
                    // Km to M
                    else if (symbol_temp1.equals("\u00B0Km") && symbol_temp2.equals("\u00B0M")) {
                        edit_temppanjang2.setText(temperaturespanjang.KilometerToMeter(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.KilometerToMeter(value_to_conversion));
                    }
                    // Km to Cm
                    else if (symbol_temp1.equals("\u00B0Km") && symbol_temp2.equals("\u00B0Cm")) {
                        edit_temppanjang2.setText(temperaturespanjang.KilometerToCentimeter(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.KilometerToCentimeter(value_to_conversion));
                    }

                    // Mm to Km
                    else if (symbol_temp1.equals("\u00B0Mm") && symbol_temp2.equals("\u00B0Km")) {
                        edit_temppanjang2.setText(temperaturespanjang.MilimeterToKilometer(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.MilimeterToKilometer(value_to_conversion));
                    }
                    // MM to M
                    else if (symbol_temp1.equals("\u00B0Mm") && symbol_temp2.equals("\u00B0M")) {
                        edit_temppanjang2.setText(temperaturespanjang.MiliToMeter(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.MiliToMeter(value_to_conversion));
                    }
                    // Mm to Cm
                    else if (symbol_temp1.equals("\u00B0Mm") && symbol_temp2.equals("\u00B0Cm")) {
                        edit_temppanjang2.setText(temperaturespanjang.MiliToCentimeter(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.MiliToCentimeter(value_to_conversion));
                    }
                    // M to Km
                    else if (symbol_temp1.equals("\u00B0M") && symbol_temp2.equals("\u00B0Km")) {
                        edit_temppanjang2.setText(temperaturespanjang.MeterToKilometer(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.MeterToKilometer(value_to_conversion));
                    }
                    // M to Mm
                    else if (symbol_temp1.equals("\u00B0M") && symbol_temp2.equals("\u00B0Mm")) {
                        edit_temppanjang2.setText(temperaturespanjang.MeterToMili(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.MeterToMili(value_to_conversion));
                    }

                    // M to Cm
                    else if (symbol_temp1.equals("\u00B0M") && symbol_temp2.equals("\u00B0Cm")) {
                        edit_temppanjang2.setText(temperaturespanjang.MiliToCentimeter(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.MiliToCentimeter(value_to_conversion));
                    }
                    // Cm to Km
                    else if (symbol_temp1.equals("\u00B0Cm") && symbol_temp2.equals("\u00B0Km")) {
                        edit_temppanjang2.setText(temperaturespanjang.CentimeterTokilometer(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.CentimeterTokilometer(value_to_conversion));
                    }
                    // Cm to Mm
                    else if (symbol_temp1.equals("\u00B0Cm") && symbol_temp2.equals("\u00B0Mm")) {
                        edit_temppanjang2.setText(temperaturespanjang.CentimeterTomili(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.CentimeterTomili(value_to_conversion));
                    }
                    // Cm to M
                    else if (symbol_temp1.equals("\u00B0Cm") && symbol_temp2.equals("\u00B0M")) {
                        edit_temppanjang2.setText(temperaturespanjang.CentimeterToMeter(value_to_conversion));
                        text_formulapanjang.setText(temperaturespanjang.CentimeterToMeter(value_to_conversion));
                    }
                    //if Km equals km
                    else if (symbol_temp1.equals("\u00B0Km") && symbol_temp2.equals("\u00B0Km")) {
                        edit_temppanjang2.setText(temperaturespanjang.check_after_decimal_point(value_to_conversion));
                        text_formulapanjang.setText("\u00B0Kg  =  " + temperaturespanjang.check_after_decimal_point(value_to_conversion));
                    }
                    //if Mm equals Mm
                    else if (symbol_temp1.equals("\u00B0Mm") && symbol_temp2.equals("\u00B0Mm")) {
                        edit_temppanjang2.setText(temperaturespanjang.check_after_decimal_point(value_to_conversion));
                        text_formulapanjang.setText("\u00B0q  =  " + temperaturespanjang.check_after_decimal_point(value_to_conversion));
                    }
                    //if M equals M
                    else if (symbol_temp1.equals("\u00B0M") && symbol_temp2.equals("\u00B0M")) {
                        edit_temppanjang2.setText(temperaturespanjang.check_after_decimal_point(value_to_conversion));
                        text_formulapanjang.setText("\u00B0T  =  " + temperaturespanjang.check_after_decimal_point(value_to_conversion));
                    }
                    //if Cm equals Cm
                    else if (symbol_temp1.equals("\u00B0Cm") && symbol_temp2.equals("\u00B0Cm")) {
                        edit_temppanjang2.setText(temperaturespanjang.check_after_decimal_point(value_to_conversion));
                        text_formulapanjang.setText("\u00B0G  =  " + temperaturespanjang.check_after_decimal_point(value_to_conversion));
                    }
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}