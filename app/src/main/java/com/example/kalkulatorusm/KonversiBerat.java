package com.example.kalkulatorusm;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kalkulatorusm.R;
import com.example.kalkulatorusm.rumus.SharedPrefsTemphberat;
import com.example.kalkulatorusm.rumus.Temperaturesberat;

import java.util.Objects;

public class KonversiBerat extends AppCompatActivity {
    ActionBar back2;

    private AppCompatEditText edit_tempberat1;
    private AppCompatEditText edit_tempberat2;
    private Temperaturesberat temperaturesberat;
    private LinearLayout layout_formulaberat;
    private TextView text_formulaberat;
    private Animation rotate_zoom_out;
    private String[] temperatures = new String[]{
            "\u00B0Kg",
            "\u00B0q",
            "\u00B0T",
            "\u00B0G",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konversi_berat);

        temperaturesberat = new Temperaturesberat(this);

        layout_formulaberat = findViewById(R.id.layout_formulaberat);
        text_formulaberat = findViewById(R.id.text_formulaberat);

        rotate_zoom_out = AnimationUtils.loadAnimation(KonversiBerat.this, R.anim.rotate_zoom_out);

        //edit text temp to conversion 1
        edit_tempberat1 = findViewById(R.id.edit_1_temperature_to_conversionberat);
        edit_tempberat1.setHint(SharedPrefsTemphberat.getTempSymbol1(KonversiBerat.this));

        //edit text temp to conversion 2
        edit_tempberat2 = findViewById(R.id.edit_2_temperature_to_conversionberat);
        edit_tempberat2.setHint(SharedPrefsTemphberat.getTempSymbol2(KonversiBerat.this));
        edit_tempberat2.setKeyListener(null);
        edit_tempberat2.setClickable(false);

        //Spinner & Adapter 1
        ArrayAdapter<String> arrayAdapter1 =
                new ArrayAdapter(KonversiBerat.this, android.R.layout.simple_spinner_item, temperatures);
        arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AppCompatSpinner appCompatSpinner1 = findViewById(R.id.spinner_1_temperature_to_conversionberat);
        appCompatSpinner1.setAdapter(arrayAdapter1);

        //set selection
        appCompatSpinner1.setSelection(SharedPrefsTemphberat.getTempIndex1(KonversiBerat.this));
        appCompatSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String temp_symbol1 = temperatures[position];
                SharedPrefsTemphberat.setTemperature1(KonversiBerat.this, temp_symbol1, position);
                edit_tempberat1.setHint(temperatures[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        back2 = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //spinner & adapter 2
        ArrayAdapter<String> arrayAdapter2 =
                new ArrayAdapter(KonversiBerat.this, android.R.layout.simple_spinner_item, temperatures);
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AppCompatSpinner appCompatSpinner2 = findViewById(R.id.spinner_2_temperature_to_conversionberat);
        appCompatSpinner2.setAdapter(arrayAdapter2);
        appCompatSpinner2.setSelection(SharedPrefsTemphberat.getTempIndex2(KonversiBerat.this));
        appCompatSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String temp_symbol2 = temperatures[position];
                SharedPrefsTemphberat.setTemperature2(KonversiBerat.this, temp_symbol2, position);
                edit_tempberat2.setHint(temperatures[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Button Count
        AppCompatButton btn_countberat = findViewById(R.id.countberat);
        btn_countberat.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (Objects.requireNonNull(edit_tempberat1.getText()).toString().isEmpty()) {

                    Toast.makeText(KonversiBerat.this,
                            "Masukan nilai suhu yang ingin di konversi", Toast.LENGTH_SHORT).show();

                } else {
                    layout_formulaberat.startAnimation(rotate_zoom_out);
                    if (layout_formulaberat.getVisibility() == View.GONE) {
                        layout_formulaberat.setVisibility(View.VISIBLE);
                    }
                    String symbol_temp1 = SharedPrefsTemphberat.getTempSymbol1(KonversiBerat.this);
                    String symbol_temp2 = SharedPrefsTemphberat.getTempSymbol2(KonversiBerat.this);
                    double value_to_conversion = Double.parseDouble(edit_tempberat1.getText().toString());

                    // Kg to G
                    if (symbol_temp1.equals("\u00B0Kg") && symbol_temp2.equals("\u00B0G")) {
                        edit_tempberat2.setText(temperaturesberat.KilogramToGram(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.KilogramToGram(value_to_conversion));
                    }
                    // Kg to q
                    else if (symbol_temp1.equals("\u00B0Kg") && symbol_temp2.equals("\u00B0q")) {
                        edit_tempberat2.setText(temperaturesberat.KilogramToKwintal(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.KilogramToKwintal(value_to_conversion));
                    }
                    // Kg to T
                    else if (symbol_temp1.equals("\u00B0Kg") && symbol_temp2.equals("\u00B0T")) {
                        edit_tempberat2.setText(temperaturesberat.KilogramToTon(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.KilogramToTon(value_to_conversion));
                    }

                    // q to Kg
                    else if (symbol_temp1.equals("\u00B0q") && symbol_temp2.equals("\u00B0Kg")) {
                        edit_tempberat2.setText(temperaturesberat.KwintalToKilogram(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.KwintalToKilogram(value_to_conversion));
                    }
                    // q to T
                    else if (symbol_temp1.equals("\u00B0q") && symbol_temp2.equals("\u00B0T")) {
                        edit_tempberat2.setText(temperaturesberat.KwintalToTon(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.KwintalToTon(value_to_conversion));
                    }
                    // q to G
                    else if (symbol_temp1.equals("\u00B0q") && symbol_temp2.equals("\u00B0G")) {
                        edit_tempberat2.setText(temperaturesberat.KwintalToGram(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.KwintalToGram(value_to_conversion));
                    }
                    // T to Kg
                    else if (symbol_temp1.equals("\u00B0T") && symbol_temp2.equals("\u00B0Kg")) {
                        edit_tempberat2.setText(temperaturesberat.TonToKilogram(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.TonToKilogram(value_to_conversion));
                    }
                    // T to q
                    else if (symbol_temp1.equals("\u00B0T") && symbol_temp2.equals("\u00B0q")) {
                        edit_tempberat2.setText(temperaturesberat.TonTokwintal(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.TonTokwintal(value_to_conversion));
                    }

                    // T to G
                    else if (symbol_temp1.equals("\u00B0T") && symbol_temp2.equals("\u00B0G")) {
                        edit_tempberat2.setText(temperaturesberat.TonToGram(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.TonToGram(value_to_conversion));
                    }
                    // G to Kg
                    else if (symbol_temp1.equals("\u00B0G") && symbol_temp2.equals("\u00B0Kg")) {
                        edit_tempberat2.setText(temperaturesberat.GramTokilogram(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.GramTokilogram(value_to_conversion));
                    }
                    // G to q
                    else if (symbol_temp1.equals("\u00B0G") && symbol_temp2.equals("\u00B0q")) {
                        edit_tempberat2.setText(temperaturesberat.GramTokwintal(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.GramTokwintal(value_to_conversion));
                    }
                    // G to T
                    else if (symbol_temp1.equals("\u00B0G") && symbol_temp2.equals("\u00B0T")) {
                        edit_tempberat2.setText(temperaturesberat.GramToTon(value_to_conversion));
                        text_formulaberat.setText(temperaturesberat.GramToTon(value_to_conversion));
                    }
                    //if Kg equals Kg
                    else if (symbol_temp1.equals("\u00B0Kg") && symbol_temp2.equals("\u00B0Kg")) {
                        edit_tempberat2.setText(temperaturesberat.check_after_decimal_point(value_to_conversion));
                        text_formulaberat.setText("\u00B0Kg  =  " + temperaturesberat.check_after_decimal_point(value_to_conversion));
                    }
                    //if q equals q
                    else if (symbol_temp1.equals("\u00B0q") && symbol_temp2.equals("\u00B0q")) {
                        edit_tempberat2.setText(temperaturesberat.check_after_decimal_point(value_to_conversion));
                        text_formulaberat.setText("\u00B0q  =  " + temperaturesberat.check_after_decimal_point(value_to_conversion));
                    }
                    //if T equals T
                    else if (symbol_temp1.equals("\u00B0T") && symbol_temp2.equals("\u00B0T")) {
                        edit_tempberat2.setText(temperaturesberat.check_after_decimal_point(value_to_conversion));
                        text_formulaberat.setText("\u00B0T  =  " + temperaturesberat.check_after_decimal_point(value_to_conversion));
                    }
                    //if G equals G
                    else if (symbol_temp1.equals("\u00B0G") && symbol_temp2.equals("\u00B0G")) {
                        edit_tempberat2.setText(temperaturesberat.check_after_decimal_point(value_to_conversion));
                        text_formulaberat.setText("\u00B0G  =  " + temperaturesberat.check_after_decimal_point(value_to_conversion));
                    }
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}