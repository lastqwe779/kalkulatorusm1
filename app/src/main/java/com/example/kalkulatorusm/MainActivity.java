package com.example.kalkulatorusm;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText username;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Login();
    }

    void Login() {
        username = (EditText) findViewById(R.id.username);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String usernameKey = username.getText().toString();

                if (usernameKey.equals("admin")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else if  (usernameKey.equals("riski")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else if (usernameKey.equals("wahyu")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else if (usernameKey.equals("yuda")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else if (usernameKey.equals("bagus")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else if (usernameKey.equals("grace")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else if (usernameKey.equals("irvan")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else if (usernameKey.equals("m idris")) {

                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Beranda.class);
                    MainActivity.this.startActivity(intent);
                    finish();

                } else {

                        //jika login gagal
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Username Masih Belum Diisi!").setNegativeButton("Retry", null).create().show();

                }
                }
        });}}